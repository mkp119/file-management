import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'login';
  isSignIn : boolean = true;
  isPasswordReset : boolean = false;
  isCreateAccount : boolean = false;
  LOGIN : string = "LOGIN";
  CREATE : string = "CREATE";
  RESET_PASSWORD : string = "RESET_PASSWORD";

  changeAction(type : string){
    this.isSignIn = false;
    this.isCreateAccount = false;
    this.isPasswordReset = false;
    switch(type){
       case this.LOGIN :
       this.isSignIn = true;
       break;
       case this.CREATE :
       this.isCreateAccount = true;
       break;
       case this.RESET_PASSWORD :
       this.isPasswordReset = true;
       break;
    }
  }
}
